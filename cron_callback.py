#!/usr/bin/python
import git
from config import config
from log import log
import os
import sys
import sh

class Repository:
    def __init__(self, path, script, remote_name):
        self.path = path
        self.script = script
        self.remote_name = remote_name
    def is_up_to_date(self):
        wasUpToDate = git.is_up_to_date(self.path, self.remote_name)
        log("Checking whether the repository at '%s' is up to date; it %s." % (wasUpToDate, "is" if wasUpToDate else "### is not ###"))
        return wasUpToDate
    def validate_sanity(self):
        if not os.path.isdir(self.path):
            raise ValueError("I'm a repository with a path of  '%s', and my path is nonexistant. :(" % self.path)
    def __repr__(self):
        return "Repository(path='%s', script='%s', remote_name='%s')" % (self.path, self.script, self.remote_name)
    @staticmethod
    def from_json(data):
        if "path" not in data:
            raise ValueError("The key 'path' must be present in all of the objects in config")
        elif "script" not in data:
            raise ValueError("The key 'script' must be present in all of the objects in config")
        return Repository(data["path"], data["script"], data["remote-name"] if "remote-name" in data else "origin")

repos = []

for repo in config["repos"]:
    repos.append(Repository.from_json(repo))

for repo in repos:
    try:
        repo.validate_sanity()
    except ValueError as e:
        print "Error validating repository entry: %s" % e.message
        log("Error validating repository entry: %s" % e.message)
        sys.exit(-1)

for repo in repos:
    if not repo.is_up_to_date():
        log("Running script of repository with path '%s'" % repo.path)
        sh.bash("-c", "cd '%s'; %s" % (repo.path, repo.script))
