# RepoWatcher
Keep an eye on your BitBucket repositories. When they change, run a script.

# Adding SSH Keys

Although this is technically unrelated, I want to record it here, just in case.

Assuming you've already generated a key `key.pem`, and added it to BitBucket (or GitHub or whatever):

1. If ssh-agent isn't already started (`ps -ef | grep [s]sh-agent`), start it with `ssh-agent /bin/bash`.
2. `ssh-add key.pem`

That's it.

# How does it work?

The plan is to use `git remote show [remote name]`.
