import sh

def run_at_path(path, command):
    return sh.bash("-c", "cd '%s'; %s" % (path, command))

def is_up_to_date(path, remote_name):
    return "out of date" not in run_at_path(path, "git remote show %s" % remote_name)
