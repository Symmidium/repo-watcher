import json

config = None
with open("config.json", "r") as f:
    config = json.loads(f.read())
