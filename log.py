from config import config

def log(msg):
    with open(config["logfile"], "a") as f:
        f.write("%s\n" % msg)
